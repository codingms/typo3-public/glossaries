<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

//
// Register plugins
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'glossaries',
    'Glossary',
    'Glossary-Extension'
);
//
// Include flex forms
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['glossaries_glossary'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'glossaries_glossary',
    'FILE:EXT:glossaries/Configuration/FlexForms/Glossary.xml'
);
//
// CSH for FlexForm
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
    'tt_content.pi_flexform.glossaries_glossary.list',
    'EXT:glossaries/Resources/Private/Language/locallang_plugin_glossary.xlf'
);
