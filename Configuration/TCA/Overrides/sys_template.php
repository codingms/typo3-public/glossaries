<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

//
// Static templates

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'glossaries',
    'Configuration/TypoScript',
    'Glossary-Extension'
);
