<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'glossaries';
$table = 'tx_glossaries_domain_model_glossarycategory';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'tstamp' => 'tstamp',
        'dividers2tabs' => true,
        'sortby' => 'sorting',
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,description,',
        'iconfile' => 'EXT:glossaries/Resources/Public/Icons/glossarycategory.svg',
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-glossaries-glossarycategory'
        ],
        'accessUtility' => CodingMs\Glossaries\Utility\AccessUtility::class,
    ],
    'types' => [
        '1' => ['showitem' => '
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_general') . ',
            information,
            title,
            slug,
            description,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_language') . ',
            sys_language_uid;;;;1-1-1,
            l10n_parent,
            l10n_diffsource,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_access') . ',
            hidden;;1
        '],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'information' => \CodingMs\AdditionalTca\Tca\Configuration::full('information', $table, $extKey),
        'sys_language_uid' => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\AdditionalTca\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\AdditionalTca\Tca\Configuration::full('endtime'),
        'title' => [
            'exclude' => false,
            'label' => $lll . '.title',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', true),
        ],
        'description' => [
            'exclude' => true,
            'label' => $lll . '.description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled]',
        ],
        'slug' => [
            'exclude' => false,
            'label' => $lll . '.slug',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('slug'),
        ],
    ],
];

if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
