<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'glossaries';
$table = 'tx_glossaries_domain_model_glossary';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,sub_title,teaser,description,',
        'iconfile' => 'EXT:glossaries/Resources/Public/Icons/glossary.svg',
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-glossaries-glossary'
        ],
        'accessUtility' => CodingMs\Glossaries\Utility\AccessUtility::class,
    ],
    'types' => [
        '1' => [
            'showitem' => '
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_general') . ',
            information,
            title,
            slug,
            sub_title,
            teaser,
            description,
            --palette--;;use_detail_view_views,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_images') . ',
            images,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_seo') . ',
            html_title,
            meta_abstract,
            meta_description,
            meta_keywords,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_relations') . ',
            categories,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_language') . ',
            sys_language_uid;;;;1-1-1,
            l10n_parent,
            l10n_diffsource,
        --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_access') . ',
            hidden;;1
        '],
    ],
    'palettes' => [
        'use_detail_view_views' => ['showitem' => 'use_detail_view, views, --linebreak--, views_day, views_week, --linebreak--, views_month, views_year, --linebreak--, views_last_update'],
    ],
    'columns' => [
        'information' => \CodingMs\AdditionalTca\Tca\Configuration::full('information', $table, $extKey),
        'sys_language_uid' => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\AdditionalTca\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\AdditionalTca\Tca\Configuration::full('endtime'),
        'title' => [
            'exclude' => false,
            'label' => $lll . '.title',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', true),
        ],
        'sub_title' => [
            'exclude' => true,
            'label' => $lll . '.sub_title',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', false),
        ],
        'teaser' => [
            'exclude' => true,
            'label' => $lll . '.teaser',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('textareaSmall', true),
        ],
        'use_detail_view' => [
            'exclude' => true,
            'label' => $lll . '.use_detail_view',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('checkbox', true),
        ],
        'description' => [
            'exclude' => true,
            'label' => $lll . '.description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'views' => [
            'exclude' => true,
            'label' => $lll . '.views',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('int'),
        ],
        'views_day' => [
            'exclude' => true,
            'label' => $lll . '.views_day',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('int'),
        ],
        'views_week' => [
            'exclude' => true,
            'label' => $lll . '.views_week',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('int'),
        ],
        'views_month' => [
            'exclude' => true,
            'label' => $lll . '.views_month',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('int'),
        ],
        'views_year' => [
            'exclude' => true,
            'label' => $lll . '.views_year',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('int'),
        ],
        'views_last_update' => [
            'exclude' => true,
            'label' => $lll . '.views_last_update',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('dateTime', false, true, '', ['dbType' => 'timestamp']),
        ],
        'images' => [
            'exclude' => true,
            'label' => $lll . '.images',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'images',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => $lll . '.images_add',
                        'showPossibleLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'images',
                        'tablenames' => 'tx_glossaries_domain_model_glossary',
                    ],
                    // custom configuration for displaying fields in the overlay/reference table
                    // to use the newsPalette and imageoverlayPalette instead of the basicoverlayPalette
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;glossariesGlossaryPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;glossariesGlossaryPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                        ]
                    ]
                ],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext']
            )
        ],
        'html_title' => [
            'exclude' => true,
            'label' => $lll . '.html_title',
            'description' => $lll . '.html_title_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', false),
        ],
        'meta_abstract' => [
            'exclude' => true,
            'label' => $lll . '.meta_abstract',
            'description' => $lll . '.meta_abstract_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', false),
        ],
        'meta_description' => [
            'exclude' => true,
            'label' => $lll . '.meta_description',
            'description' => $lll . '.meta_description_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', false),
        ],
        'meta_keywords' => [
            'exclude' => true,
            'label' => $lll . '.meta_keywords',
            'description' => $lll . '.meta_keywords_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', false),
        ],
        'categories' => [
            'exclude' => true,
            'label' => $lll . '.categories',
            'description' => $lll . '.categories_description',
            'config' => \CodingMs\Glossaries\Tca\Configuration::get('categories', false),
        ],
        'slug' => [
            'exclude' => false,
            'label' => $lll . '.slug',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('slug'),
        ],
    ],
];

if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
    $return['columns']['images']['config']['foreign_match_fields']['table_local'] = 'sys_file';
}
return $return;
