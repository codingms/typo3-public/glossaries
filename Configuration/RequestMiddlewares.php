<?php

return [
    'frontend' => [
        'codingms/glossaries' => [
            'target' => \CodingMs\Glossaries\Middleware\GlossariesMiddleware::class,
            'after' => [
                'typo3/cms-frontend/backend-user-authentication',
                'typo3/cms-frontend/page-resolver',
            ],
        ],
    ],
];
