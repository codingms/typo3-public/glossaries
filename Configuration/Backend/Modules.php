<?php

$return['glossaries_glossaries'] = [
    'parent' => 'web',
    'position' => [],
    'access' => 'user',
    'iconIdentifier' => 'module-glossaries',
    'path' => '/module/glossaries/glossaries',
    'labels' => 'LLL:EXT:glossaries/Resources/Private/Language/locallang_glossaries.xlf',
    'extensionName' => 'Glossaries',
    'controllerActions' => [
        \CodingMs\Glossaries\Controller\BackendController::class => [
            'overviewGlossaries',
            'overviewCategories',
        ]
    ],
];
return $return;
