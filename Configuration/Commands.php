<?php

return [
    'glossaries:updateViewCount' => [
        'class' => \CodingMs\Glossaries\Command\UpdateViewCountCommand::class,
    ],
];
