<?php

declare(strict_types=1);

return [
    \CodingMs\Glossaries\Domain\Model\FileReference::class => [
        'tableName' => 'sys_file_reference',
    ],
];
