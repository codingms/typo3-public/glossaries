# Glossaries Extension for TYPO3

The glossaries extension for TYPO3 enables you to create and manage your website glossar.


**Features:**

*   Glossary list view
*   Optional detail view (SEO optimized, with meta and OG tags)
*   Internal hit counter of total number of visits to the glossary detail pages (list sorting by clicks possible)
*   Images and categories for glossary entries
*   Backend module for managing glossary entries incl. extended authorizations
*   Preview images / og-tag images can be selected separately
*   Page-Type container for glossary entries
*   Can also be easily integrated into an intranet to display definitions of terms

If you need some additional or custom feature - get in contact!


**Links:**

*   [Glossaries Documentation](https://www.coding.ms/documentation/typo3-glossaries "Glossaries Documentation")
*   [Glossaries Bug-Tracker](https://gitlab.com/codingms/typo3-public/glossaries/-/issues "Glossaries Bug-Tracker")
*   [Glossaries Repository](https://gitlab.com/codingms/typo3-public/glossaries "Glossaries Repository")
*   [TYPO3 Glossaries Productdetails](https://www.coding.ms/typo3-extensions/typo3-glossaries/ "TYPO3 Glossaries Productdetails")
*   [TYPO3 Glossaries Documentation](https://www.coding.ms/dokumentation/typo3-glossaries "TYPO3 Glossaries Documentation")
