# Glossaries Erweiterung für TYPO3

Die Glossaries Erweiterung für TYPO3 ermöglicht es Dir ein Website-Glossar zu erstellen und zu verwalten.


**Features:**

*   Glossar-Listenansicht
*   Optionale Detailansicht (SEO optimiert, mit Meta- und OG-Tags)
*   Interner Zähler für die Summe der Aufrufe der Glossar-Detailseiten (Listen-Sortierung nach Aufrufen möglich)
*   Bilder und Kategorisierungen für Glossar-Einträge
*   Backend-Modul zur Pflege von Glossar-Einträgen inkl. erweiterter Autorisationen
*   Preview image / og-tag image separat markierbar
*   Page-Type Container für Glossareinträge
*   Lässt sich auch einfach in ein Intranet zur Anzeige von Begriffserklärungen integrieren

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Glossaries Dokumentation](https://www.coding.ms/documentation/typo3-glossaries "Glossaries Dokumentation")
*   [Glossaries Bug-Tracker](https://gitlab.com/codingms/typo3-public/glossaries/-/issues "Glossaries Bug-Tracker")
*   [Glossaries Repository](https://gitlab.com/codingms/typo3-public/glossaries "Glossaries Repository")
*   [TYPO3 Glossaries Produktdetails](https://www.coding.ms/de/typo3-extensions/typo3-glossaries/ "TYPO3 Glossaries Produktdetails")
*   [TYPO3 Glossaries Dokumentation](https://www.coding.ms/de/documentation/typo3-glossaries "TYPO3 Glossaries Dokumentation")
