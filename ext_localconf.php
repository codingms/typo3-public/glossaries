<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Glossaries',
    'Glossary',
    [\CodingMs\Glossaries\Controller\GlossaryController::class => 'list, show'],
    []
);
//
// Page TypoScript
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '@import "EXT:glossaries/Configuration/PageTS/tsconfig.typoscript"'
);
//
// Allow backend users to drag and drop the new page type:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
    'options.pageTree.doktypesToShowInNewPageDragArea := addToList(1659712479)'
);
// Backend TypoScript
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(
    '@import "EXT:glossaries/Configuration/TypoScript/Backend/setup.typoscript"'
);

