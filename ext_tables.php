<?php

use TYPO3\CMS\Core\DataHandling\PageDoktypeRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (!defined('TYPO3')) {
    die('Access denied.');
}

//
// Registers a Backend Module
if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'Glossaries',
        'web',
        'glossaries',
        '',
        [
            \CodingMs\Glossaries\Controller\BackendController::class => 'overviewGlossaries, overviewCategories',
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:glossaries/Resources/Public/Icons/module-glossaries.svg',
            'iconIdentifier' => 'module-glossaries',
            'labels' => 'LLL:EXT:glossaries/Resources/Private/Language/locallang_glossaries.xlf',
        ]
    );
    $GLOBALS['PAGES_TYPES'][1659712479] = [
        'type' => 'web',
        // ATTENTION: Don't insert line breaks into the "allowedTables" - this will break this functionality!
        'allowedTables' => implode(
            ',',
            [
                'tx_glossaries_domain_model_glossary',
                'tx_glossaries_domain_model_glossarycategory',
                'sys_file_reference',
                'pages',
            ]
        ),
    ];
} else {
    // Page type for v12
    $dokTypeRegistry = GeneralUtility::makeInstance(PageDoktypeRegistry::class);
    $dokTypeRegistry->add(
        1659712479,
        [
            'type' => 'web',
            'allowedTables' => implode(
                ',',
                [
                    'tx_glossaries_domain_model_glossary',
                    'tx_glossaries_domain_model_glossarycategory',
                    'sys_file_reference',
                    'pages',
                ]
            ),
        ]
    );
}
//
// register svg icons: identifier and filename
$iconsSvg = [
    'mimetypes-x-content-glossaries-glossary' => 'Resources/Public/Icons/glossary.svg',
    'mimetypes-x-content-glossaries-glossarycategory' => 'Resources/Public/Icons/glossarycategory.svg',
    'icon-content-plugin-glossaries-glossary' => 'Resources/Public/Icons/glossary.svg',
    'module-glossaries' => 'Resources/Public/Icons/module-glossaries.svg',
    'apps-pagetree-glossaries' => 'Resources/Public/Icons/glossary.svg',
];
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
foreach ($iconsSvg as $identifier => $path) {
    $iconRegistry->registerIcon(
        $identifier,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:glossaries/' . $path]
    );
}
//
// Authorizations
$GLOBALS['TYPO3_CONF_VARS']['BE']['customPermOptions']['modules_glossary'] =
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \CodingMs\Glossaries\Domain\DataTransferObject\GlossaryActionPermission::class
    );
$GLOBALS['TYPO3_CONF_VARS']['BE']['customPermOptions']['modules_glossary_category'] =
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \CodingMs\Glossaries\Domain\DataTransferObject\GlossaryCategoryActionPermission::class
    );
