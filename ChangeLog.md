# Glossaries Change-Log

*	[BUGFIX] Fix translation for documentation title
*	[TASK] Migrate TypoScript imports



## 2024-07-23 Release of version 4.0.4

*	[TASK] Backend filter migration
*	[BUGFIX] Fix creating translated record container and migrate allowedTables
*	[BUGFIX] Fix backend module authorization in TYPO3 12
*	[TASK] Optimize version conditions in PHP code
*	[TASK] Migrate controller modulePrefix initialization in backend module to TYPO3 12
*	[TASK] Optimize labels in user authorizations



## 2023-11-07 Release of version 4.0.3

*	[BUGFIX] Fix PHP 7.4 support



## 2023-11-01 Release of version 4.0.2

*	[TASK] Clean up documentation



## 2023-10-31 Release of version 4.0.1

*	[BUGFIX] Fix get content object for TYPO3 11



## 2023-10-11  Release of version 4.0.0

*	[BUGFIX] Fix Unknown column table_local in where clause
*	[BUGFIX] Clean up backend module icon and registration
*	[TASK] Optimize documentation articles
*	[TASK] Migrate to TYPO3 12 and remove support for TYPO3 10



## 2023-02-15 Release of version 3.4.1

*	[TASK] Generate documentation for TypoScript constants
*	[TASK] Migrate TYPO3 PHP constants
*	[TASK] Migrate CSH to TCA description
*	[TASK] Add missing Change log entries from parallel releases



## 2022-11-20 Release of version 3.4.0

*	[FEATURE] Define access utility for backend management
*	[TASK] Add meta description to config.json
*	[TASK] Replace EditInModal by Edit in backend module
*	[TASK] Migrate backend module header and footer



## 2022-11-21 Release of version 3.3.0

*	[FEATURE] Menu processor
*	[TASK] Add some more documentation



## 2022-08-05 Release of version 3.2.0

*	[TASK] Preparations for public release
*	[TASK] Migrate page.contains to page.type



## 2022-06-03 Release of version 3.1.1

*	[BUGFIX] Fix backend authorization naming



## 2022-04-29 Release of version 3.1.0

*	[TASK] Add backend search word filter
*	[BUGFIX] Fix backend module initialisation
*	[TASK] Redirect to default list page, if detail page has no parameters
*	[TASK] Remove objectmanager usage for image-service
*	[TASK] Remove unused showRemovedLocalizationRecords from TCA



## 2022-04-02 Release of version 3.0.0

*	[BUGFIX] Fix undefined index in PHP 8 Middleware
*	[TASK] Migrate backend module and backend authorizations
*	[TASK] Refactoring for PHP 8.0 support
*	[TASK] Sync translated DE *.xlf files
*	[TASK] Add translated DE *.xlf files
*	[TASK] Migration for TYPO3 11.5 - remove support for TYPO3 9.5



## 2021-08-18 Release of version 2.3.1

*	[TASK] Add menu processor documentation



## 2021-08-18 Release of version 2.3.1

*	[TASK] Add menu processor documentation



## 2021-05-01 Release of version 2.2.0

*	[TASK] Add slug field to glossary record
*	[BUGFIX] Fix documentation configuration
*	[BUGFIX] Fix Middleware
*	[FEATURE] View counter with psr-15 middleware
*	[TASK] Add documentation for sitemap xml configuration
*	[TASK] Add documentations configuration



## 2021-05-01 Release of version 2.1.4

*	[TASK] Add extension key in composer.json



## 2020-10-14 Release of version 2.1.3

*	[TASK] Add translation files



## 2020-10-04 Release of version 2.1.2

*	[BUGFIX] Rise additional_tca requirement



## 2020-08-25 Release of version 2.1.1

*	[BUGFIX] Fix translation values in backend module



## 2020-08-24 Release of version 2.1.0

*	[FEATURE] Add slug field for categories
*	[TASK] Add documentation for human readable URLs



## 2020-08-10 Release of version 2.0.0

*	[TASK] Add additional_tca support
*	[TASK] Optimize page title provider configuration
*	[TASK] Add extra tags in composer.json
*	[TASK] Preparation for migration to TYPO3 10
*	[TASK] Refactor TypoScript constants
*	[TASK] Add optional check box for preview/og-tag image
*	[TASK] Remove deprecated functions
*	[TASK] Remove realurl auto configuration hook
*	[TASK] Rise version for migrating to TYPO3 9-10



## 2019-12-02 Release of version 1.2.0

*	[TASK] Backendcontroller refactoring.
*	[TASK] TCA refactoring.
*	[FEATURE] Add category filter.
*	[TASK] Change description column with teaser column in backend module.
*	[TASK] Optimize Fluid templates and add microdata.
*	[TASK] Optimize Namespaces.
*	[FEATURE] Add backend module for managing records.
*	[BUGFIX] Fix namespace issue in Glossary model.



## 2019-10-13 Release of version 1.1.1

*	[TASK] Remove DEV identifier.
*	[TASK] Add Gitlab-CI configuration.



## 2018-10-09 Release of version 1.1.0

*	[TASK] Adding license information.
*	[TASK] Adding counter for glossary entry views.
*	[TASK] Adding composer.json.
*	[TASK] Cleaning up sources.
*	[TASK] Cleaning up TCA.
*	[FEATURE] Adding images to glossary entries.
*	[FEATURE] Adding a limit of items for list view.
*	[FEATURE] Adding a sorting setting for list view.
*	[FEATURE] Adding a display setting so that it's possible to show always the list or detail in plugin.
*	[TASK] Adding documentation for Realurl.
*	[TASK] Migrating icons into SVG by using the Icon factory.
