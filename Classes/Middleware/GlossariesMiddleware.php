<?php

namespace CodingMs\Glossaries\Middleware;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Glossaries\Service\ViewCountService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class GlossariesMiddleware
 */
class GlossariesMiddleware implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (isset($GLOBALS['BE_USER'])) {
            return $handler->handle($request);
        }
        $queryParams = $request->getQueryParams();
        if (isset($queryParams['tx_glossaries_glossary'])) {
            $action = $queryParams['tx_glossaries_glossary']['action'] ?? 'list';
            $controller = $queryParams['tx_glossaries_glossary']['controller'] ?? 'Glossary';
            $glossary = intval($queryParams['tx_glossaries_glossary']['glossary'] ?? 0);
            $routing = $request->getAttribute('routing');
            if ($routing instanceof PageArguments) {
                $pid = $routing->getPageId();
            } else {
                $pid = 0;
            }

            if ($action === 'show' && $controller === 'Glossary' && $glossary > 0 && $pid > 0) {
                /** @var ConnectionPool $connectionPool */
                $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
                $ttContentQueryBuilder = $connectionPool->getConnectionForTable('tt_content')->createQueryBuilder();
                $pluginCount = $ttContentQueryBuilder
                    ->count('uid')
                    ->from('tt_content')
                    ->where($ttContentQueryBuilder->expr()->eq(
                        'pid',
                        $ttContentQueryBuilder->createNamedParameter($pid)
                    ))
                    ->andWhere($ttContentQueryBuilder->expr()->eq(
                        'CType',
                        $ttContentQueryBuilder->createNamedParameter('list')
                    ))
                    ->andWhere($ttContentQueryBuilder->expr()->eq(
                        'list_type',
                        $ttContentQueryBuilder->createNamedParameter('glossaries_glossary')
                    ))
                    ->execute()
                    ->fetchOne();
                if ($pluginCount > 0) {
                    $glossariesCounterQueryBuilder = $connectionPool->getConnectionForTable('tx_glossaries_domain_model_glossaryclickcount')->createQueryBuilder();
                    $glossariesCounterQueryBuilder
                        ->insert('tx_glossaries_domain_model_glossaryclickcount')
                        ->values([
                            'glossary' => $glossary,
                            'click_timestamp' => time(),
                        ])
                        ->execute();

                    ViewCountService::updateViewerCountForGlossary($glossary);
                }
            }
        }
        return $handler->handle($request);
    }
}
