<?php

namespace CodingMs\Glossaries\Service;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ViewCountService
 */
class ViewCountService
{
    const DAY = 60 * 60 * 24;
    const WEEK = self::DAY * 7;
    const MONTH = self::DAY * 30;
    const YEAR = self::DAY * 365;

    /**
     * @param int $glossary
     */
    public static function updateViewerCountForGlossary(int $glossary)
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);

        $glossariesCounterQueryBuilder = $connectionPool->getConnectionForTable('tx_glossaries_domain_model_glossaryclickcount')->createQueryBuilder();
        $totalTimestamps = $glossariesCounterQueryBuilder
            ->select('click_timestamp')
            ->from('tx_glossaries_domain_model_glossaryclickcount')
            ->where($glossariesCounterQueryBuilder->expr()->eq(
                'glossary',
                $glossariesCounterQueryBuilder->createNamedParameter($glossary)
            ))
            ->execute()
            ->fetchFirstColumn();

        $time = time();
        $yearTimestamps = array_filter($totalTimestamps, function ($timestamp) use ($time) {return $timestamp > $time - self::YEAR;});
        $monthTimestamps = array_filter($yearTimestamps, function ($timestamp) use ($time) {return $timestamp > $time - self::MONTH;});
        $weekTimestamps = array_filter($monthTimestamps, function ($timestamp) use ($time) {return $timestamp > $time - self::WEEK;});
        $dayTimestamps = array_filter($weekTimestamps, function ($timestamp) use ($time) {return $timestamp > $time - self::DAY;});

        $totalViews = count($totalTimestamps);
        $yearViews = count($yearTimestamps);
        $monthViews = count($monthTimestamps);
        $weekViews = count($weekTimestamps);
        $dayViews = count($dayTimestamps);

        $glossariesQueryBuilder = $connectionPool->getConnectionForTable('tx_glossaries_domain_model_glossary')->createQueryBuilder();
        $glossariesQueryBuilder
            ->update('tx_glossaries_domain_model_glossary')
            ->where($glossariesQueryBuilder->expr()->eq(
                'uid',
                $glossariesQueryBuilder->createNamedParameter($glossary)
            ))
            ->set('views', $totalViews)
            ->set('views_day', $dayViews)
            ->set('views_week', $weekViews)
            ->set('views_month', $monthViews)
            ->set('views_year', $yearViews)
            ->set('views_last_update', $time)
            ->execute();
    }
}
