<?php

namespace CodingMs\Glossaries\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Glossaries\Domain\Model\FileReference;
use CodingMs\Glossaries\Domain\Model\Glossary;
use CodingMs\Glossaries\Domain\Model\GlossaryCategory;
use CodingMs\Glossaries\Domain\Repository\GlossaryCategoryRepository;
use CodingMs\Glossaries\Domain\Repository\GlossaryRepository;
use CodingMs\Glossaries\PageTitle\PageTitleProvider;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerInterface;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @noinspection PhpUnused
 */
class GlossaryController extends ActionController
{
    protected GlossaryRepository $glossaryRepository;
    protected GlossaryCategoryRepository $glossaryCategoryRepository;

    /**
     * @param GlossaryRepository $glossaryRepository
     * @param GlossaryCategoryRepository $glossaryCategoryRepository
     */
    public function __construct(
        GlossaryRepository $glossaryRepository,
        GlossaryCategoryRepository $glossaryCategoryRepository
    ) {
        $this->glossaryRepository = $glossaryRepository;
        $this->glossaryCategoryRepository = $glossaryCategoryRepository;
    }

    /**
     * Selected Category-Object, if there is a selected one
     * @var GlossaryCategory
     */
    protected GlossaryCategory $selectedCategory;

    /**
     * @var array<string, mixed>
     */
    protected array $content = [];

    /**
     * Initialize view
     *
     * @param $view
     * @return void
     */
    /* @phpstan-ignore-next-line */
    public function initializeView($view){
        //
        // Get content data
        if((int)VersionNumberUtility::getCurrentTypo3Version() > 11) {
            /** @var ContentObjectRenderer $contentObject */
            $contentObject = $this->request->getAttribute('currentContentObject');
        } else {
            /** @var ContentObjectRenderer $contentObject */
            $contentObject = $this->configurationManager->getContentObject();
        }
        $this->content = $contentObject->data;
        $this->view->assign('content', $this->content);
    }

    /**
     * action list
     *
     * @return ResponseInterface
     * @throws InvalidQueryException
     */
    public function listAction(): ResponseInterface
    {
        // If only the details should be displayed, perform a forward to show action
        if ($this->settings['display'] === 'AlwaysDetail') {
            if ($this->request->hasArgument('glossary')) {
                $this->redirect(
                    'show',
                    null,
                    null,
                    ['glossary' => $this->request->getArgument('glossary')],
                    $this->settings['detail']['defaultPid']
                );
            } else {
                $this->addFlashMessage(
                    (string)LocalizationUtility::translate('tx_glossaries_message.error_glossary_not_found', 'glossaries'),
                    '',
                    AbstractMessage::ERROR
                );
            }
        }
        // Is there a category selected?
        if ($this->request->hasArgument('selectedCategory')) {
            $selectedCategoryUid = (int)$this->request->getArgument('selectedCategory');
            if ($selectedCategoryUid > 0) {
                $selectedCategory = $this->glossaryCategoryRepository->findByIdentifier($selectedCategoryUid);
                if ($selectedCategory instanceof GlossaryCategory) {
                    $this->selectedCategory = $selectedCategory;
                }
            }
        }
        //
        // Get all available categories
        $allCategories = [];
        // In case of selected categories
        if (trim($this->settings['showFromCategories']) != '') {
            $allCategoriesUids = explode(',', $this->settings['showFromCategories']);
            foreach ($allCategoriesUids as $allCategoriesUid) {
                $allCategory = $this->glossaryCategoryRepository->findByIdentifier($allCategoriesUid);
                if ($allCategory instanceof GlossaryCategory) {
                    $allCategories[] = $allCategory;
                    // Already a category selected?!
                    if (!isset($this->selectedCategory)) {
                        $this->selectedCategory = $allCategory;
                    }
                }
            }
        } // otherwise use all accessible categories
        else {
            $allCategories = $this->glossaryCategoryRepository->findAll();
            if ($allCategories->count() > 0) {
                // Already a category selected?!
                if (!isset($this->selectedCategory)) {
                    $this->selectedCategory = $allCategories->getFirst();
                }
            }
        }
        //
        $params = [
            'limit' => (int)$this->settings['list']['limit'],
            'sortBy' => $this->settings['list']['sortBy'],
            'ignoreCategories' => (bool)$this->settings['ignoreCategories']
        ];
        $glossaryEntries = $this->glossaryRepository->findAllBySearch($params, $this->selectedCategory);
        //
        $this->view->assign('glossaryEntries', $glossaryEntries);
        $this->view->assign('glossaryCategories', $allCategories);
        $this->view->assign('settings', $this->settings);
        $this->view->assign('selectedCategory', $this->selectedCategory);
        return $this->htmlResponse();
    }

    /**
     * Shows a single glossary

     * @return ResponseInterface
     */
    public function showAction(): ResponseInterface
    {
        $glossary = null;
        if ($this->request->hasArgument('glossary')) {
            $glossaryUid = (int)$this->request->getArgument('glossary');
            if ($glossaryUid > 0) {
                $selectedGlossary = $this->glossaryRepository->findByIdentifier($glossaryUid);
                if ($selectedGlossary instanceof Glossary) {
                    $glossary = $selectedGlossary;
                }
            }
        }
        if (!isset($glossary)) {
            $this->addFlashMessage(
                (string)LocalizationUtility::translate('tx_glossaries_message.error_glossary_not_found', 'glossaries'),
                '',
                AbstractMessage::ERROR
            );
            $this->redirect('list', null, null, [], $this->settings['list']['defaultPid']);
        }
        // Is there a category selected?
        if ($this->request->hasArgument('selectedCategory')) {
            $selectedCategoryUid = (int)$this->request->getArgument('selectedCategory');
            if ($selectedCategoryUid > 0) {
                $selectedCategory = $this->glossaryCategoryRepository->findByIdentifier($selectedCategoryUid);
                if ($selectedCategory instanceof GlossaryCategory) {
                    $this->selectedCategory = $selectedCategory;
                }
            }
        }
        // If only the list should be displayed, perform a forward to list action
        if ($this->settings['display'] === 'AlwaysList') {
            $this->redirect('list', null, null, [], $this->settings['list']['defaultPid']);
        }
        // Set Title and Meta tags
        $this->injectMetaInformation($glossary, $this->settings['siteName']);
        // Display glossary
        $this->view->assign('glossaryEntry', $glossary);
        $this->view->assign('settings', $this->settings);
        $this->view->assign('selectedCategory', $this->selectedCategory);
        return $this->htmlResponse();
    }

    /**
     * @param Glossary $glossary
     * @param string $siteName
     * @return ResponseInterface
     */
    protected function injectMetaInformation(Glossary $glossary, string $siteName = ''): ResponseInterface
    {
        //
        // Get fallback values
        $htmlTitle = trim($glossary->getHtmlTitle());
        if ($htmlTitle === '') {
            $htmlTitle = $glossary->getTitle();
        }
        $description = trim($glossary->getMetaDescription());
        if ($description === '') {
            $description = substr(strip_tags($glossary->getDescription()), 0, 160);
        }
        //
        /** @var MetaTagManagerRegistry $metaTagManager */
        $metaTagManagerRegistry = GeneralUtility::makeInstance(MetaTagManagerRegistry::class);
        //
        /** @var MetaTagManagerInterface $metaTagManagerDescription */
        $metaTagManagerDescription = $metaTagManagerRegistry->getManagerForProperty('description');
        $metaTagManagerDescription->addProperty('description', $description);
        //
        /** @var MetaTagManagerInterface $metaTagManagerAbstract */
        $metaTagManagerAbstract = $metaTagManagerRegistry->getManagerForProperty('abstract');
        $metaTagManagerAbstract->addProperty('abstract', $glossary->getMetaAbstract());
        //
        /** @var MetaTagManagerInterface $metaTagManagerKeywords */
        $metaTagManagerKeywords = $metaTagManagerRegistry->getManagerForProperty('keywords');
        $metaTagManagerKeywords->addProperty('keywords', $glossary->getMetaKeywords());
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgTitle */
        $metaTagManagerOgTitle = $metaTagManagerRegistry->getManagerForProperty('og:title');
        $metaTagManagerOgTitle->addProperty('og:title', $htmlTitle);
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgDescription */
        $metaTagManagerOgDescription = $metaTagManagerRegistry->getManagerForProperty('og:description');
        $metaTagManagerOgDescription->addProperty('og:description', $description);
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgType */
        $metaTagManagerOgType = $metaTagManagerRegistry->getManagerForProperty('og:type');
        $metaTagManagerOgType->addProperty('og:type', 'WebPage');
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgSiteName */
        $metaTagManagerOgSiteName = $metaTagManagerRegistry->getManagerForProperty('og:site_name');
        $metaTagManagerOgSiteName->addProperty('og:site_name', $siteName);
        if ($siteName === '{$themes.configuration.siteName}') {
            $messageBody = 'Please set TypoScript constant {$themes.configuration.siteName} with sitename for og:site_name';
            $messageTitle = 'Error';
            $this->addFlashMessage($messageBody, $messageTitle, AbstractMessage::ERROR);
        }
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgUrl */
        $metaTagManagerOgUrl = $metaTagManagerRegistry->getManagerForProperty('og:url');
        $metaTagManagerOgUrl->addProperty('og:url', GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
        //
        // Preview image
        $previewImages = $glossary->getImagesPreviewOnly();
        if (count($previewImages) > 0) {
            /** @var FileReference $previewImage */
            $previewImage = $previewImages[0];
            /** @var ImageService $imageService */
            $imageService = GeneralUtility::makeInstance(ImageService::class);
            $image = $imageService->getImage($previewImage, $previewImage, true);
            //
            // Cropping
            if ($image->hasProperty('crop') && $image->getProperty('crop')) {
                $cropString = $image->getProperty('crop');
            }
            $cropVariantCollection = CropVariantCollection::create($cropString ?? '');
            $cropVariant = 'default';
            $cropArea = $cropVariantCollection->getCropArea($cropVariant);
            $processingInstructions = [
                'width' => 600,
                'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
            ];
            //
            $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
            $imageUrl = $imageService->getImageUri($processedImage, true);
            //
            /** @var MetaTagManagerInterface $metaTagManagerOgImage */
            $metaTagManagerOgImage = $metaTagManagerRegistry->getManagerForProperty('og:image');
            $metaTagManagerOgImage->addProperty('og:image', $imageUrl);
        }
        //
        /** @var PageTitleProvider $seoTitlePageTitleProvider */
        $seoTitlePageTitleProvider = GeneralUtility::makeInstance(PageTitleProvider::class);
        $seoTitlePageTitleProvider->setTitle($htmlTitle);
        return $this->htmlResponse();
    }
}
