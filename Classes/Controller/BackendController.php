<?php

namespace CodingMs\Glossaries\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Glossaries\Controller\Backend\Traits\Filter\SearchWordTrait;
use CodingMs\Glossaries\Domain\DataTransferObject\GlossaryActionPermission;
use CodingMs\Glossaries\Domain\DataTransferObject\GlossaryCategoryActionPermission;
use CodingMs\Glossaries\Domain\Model\GlossaryCategory;
use CodingMs\Glossaries\Domain\Repository\GlossaryCategoryRepository;
use CodingMs\Glossaries\Domain\Repository\GlossaryRepository;
use CodingMs\Modules\Controller\BackendController as BaseBackendController;
use CodingMs\Modules\Utility\BackendListUtility;
use Exception;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Backend\Routing\UriBuilder as UriBuilderBackend;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * BackendController
 */
class BackendController extends BaseBackendController
{
    use SearchWordTrait;

    protected GlossaryRepository $glossaryRepository;
    protected GlossaryCategoryRepository $glossaryCategoryRepository;

    /**
     * @param TypoScriptService $typoScriptService
     * @param BackendListUtility $backendListUtility
     * @param UriBuilderBackend $uriBuilderBackend
     * @param ModuleTemplateFactory $moduleTemplateFactory
     * @param GlossaryRepository $glossaryRepository
     * @param GlossaryCategoryRepository $glossaryCategoryRepository
     */
    public function __construct(
        TypoScriptService $typoScriptService,
        BackendListUtility $backendListUtility,
        UriBuilderBackend $uriBuilderBackend,
        ModuleTemplateFactory $moduleTemplateFactory,
        GlossaryRepository $glossaryRepository,
        GlossaryCategoryRepository $glossaryCategoryRepository
    ) {
        parent::__construct(
            $typoScriptService,
            $backendListUtility,
            $uriBuilderBackend,
            $moduleTemplateFactory
        );
        //
        $this->moduleName = 'glossaries_glossaries';
        $this->modulePrefix = 'tx_glossaries_glossariesglossaries';
        if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
            $this->moduleName = 'web_GlossariesGlossaries';
            $this->modulePrefix = 'tx_glossaries_web_glossariesglossaries';
        }
        $this->extensionName = 'Glossaries';
        //
        $this->glossaryRepository = $glossaryRepository;
        $this->glossaryCategoryRepository = $glossaryCategoryRepository;
    }

    /**
     * @return void
     * @throws Exception
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();
        $this->createMenu();
        $this->createButtons();
    }

    /**
     * Create action menu
     * @return void
     */
    protected function createMenu(): void
    {
        $actions = [
            [
                'action' => 'overviewGlossaries',
                'label' => LocalizationUtility::translate('tx_glossaries_label.menu_overview_glossaries', $this->extensionName)
            ]
        ];
        if (GlossaryCategoryActionPermission::listCategoriesAllowed()) {
            $actions[] = [
                'action' => 'overviewCategories',
                'label' => LocalizationUtility::translate('tx_glossaries_label.menu_overview_categories', $this->extensionName)
            ];
        } elseif (!GlossaryCategoryActionPermission::listCategoriesAllowed() && isset($_SERVER['DDEV_HOSTNAME'])) {
            $actions[] = [
                'action' => 'overviewCategories',
                'label' => LocalizationUtility::translate('tx_glossaries_label.menu_overview_categories', $this->extensionName) . ' [disallowed]'
            ];
        }
        $this->createMenuActions($actions);
    }

    /**
     * Add menu buttons for specific actions
     * @return void
     * @throws Exception
     */
    protected function createButtons(): void
    {
        $buttonBar = $this->moduleTemplate->getDocHeaderComponent()->getButtonBar();
        switch ($this->request->getControllerActionName()) {
            case 'overviewGlossaries':
                // New
                $this->getButton(
                    $buttonBar,
                    'new',
                    [
                        'translationKey' => 'new_glossary',
                        'table' => 'tx_glossaries_domain_model_glossary'
                    ],
                    !GlossaryActionPermission::glossaryCreationAllowed()
                );
                //
                $this->getButton($buttonBar, 'refresh', [
                    'translationKey' => 'list_overview_glossaries_refresh'
                ]);
                $this->getButton($buttonBar, 'bookmark', [
                    'translationKey' => 'list_overview_glossaries_bookmark'
                ]);
                break;
            case 'overviewCategories':
                // New
                $this->getButton(
                    $buttonBar,
                    'new',
                    [
                        'translationKey' => 'new_category',
                        'table' => 'tx_glossaries_domain_model_glossarycategory'
                    ],
                    !GlossaryCategoryActionPermission::categoryCreationAllowed()
                );
                //
                $this->getButton($buttonBar, 'refresh', [
                    'translationKey' => 'list_overview_categories_refresh'
                ]);
                $this->getButton($buttonBar, 'bookmark', [
                    'translationKey' => 'list_overview_categories_bookmark'
                ]);
                break;
        }
    }

    /**
     * Glossaries overview
     *
     * @throws NoSuchArgumentException
     * @throws InvalidQueryException
     * @noinspection PhpUnused
     */
    public function overviewGlossariesAction(): ResponseInterface
    {
        // Build list
        $list = $this->backendListUtility->initList(
            $this->settings['lists']['glossaries'],
            $this->request,
            ['category', 'searchWord']
        );
        // Initialize filtering
        $list['category']['items'] = [];
        /** @var GlossaryCategory $category */
        foreach ($this->glossaryCategoryRepository->findAll() as $category) {
            $list['category']['items'][$category->getUid()] = $category->getTitle();
        }
        // Selected category
        if ($this->request->hasArgument('category')) {
            $list['category']['selected'] = (int)$this->request->getArgument('category');
        }
        $list = $this->processSearchWordFilterTrait($list);
        // Allow delete?!
        if (!GlossaryActionPermission::glossaryDeletionAllowed()) {
            if ($list['authorization']['isDdev']) {
                $list['actions']['delete']['css'] = 'bg-danger';
            } else {
                $list['actions']['delete']['css'] = 'disabled';
            }
        }
        // Store settings
        $this->backendListUtility->writeSettings($list['id'], $list);
        // Export Result as CSV!?
        if ($this->request->hasArgument('csv')) {

            $list['limit'] = 0;
            $list['offset'] = 0;
            $list['pid'] = $this->pageUid;
            $glossaries = $this->glossaryRepository->findAllForBackendList($list);
            $list['countAll'] = $this->glossaryRepository->findAllForBackendList($list, true);
            $this->backendListUtility->exportAsCsv($glossaries, $list);
        } else {

            $list['pid'] = $this->pageUid;
            $glossaries = $this->glossaryRepository->findAllForBackendList($list);
            $list['countAll'] = $this->glossaryRepository->findAllForBackendList($list, true);
        }
        //
        $this->getViewToUse()->assign('list', $list);
        $this->getViewToUse()->assign('glossaries', $glossaries);
        $this->getViewToUse()->assign('currentPage', $this->pageUid);
        $this->getViewToUse()->assign('actionMethodName', $this->actionMethodName);
        return $this->renderViewToUse();
    }

    /**
     * Categories overview
     *
     * @throws NoSuchArgumentException
     * @noinspection PhpUnused
     */
    public function overviewCategoriesAction(): ResponseInterface
    {
        if (!GlossaryCategoryActionPermission::listCategoriesAllowed()) {
            $this->redirect('overviewGlossaries');
        }
        // Build list
        $list = $this->backendListUtility->initList(
            $this->settings['lists']['categories'],
            $this->request
        );
        // Allow delete?!
        if (!GlossaryCategoryActionPermission::categoryDeletionAllowed()) {
            if ($list['authorization']['isDdev']) {
                $list['actions']['delete']['css'] = 'bg-danger';
            } else {
                $list['actions']['delete']['css'] = 'disabled';
            }
        }
        // Store settings
        $this->backendListUtility->writeSettings($list['id'], $list);
        // Export Result as CSV!?
        if ($this->request->hasArgument('csv')) {
            $list['limit'] = 0;
            $list['offset'] = 0;
            $list['pid'] = $this->pageUid;
            $categories = $this->glossaryCategoryRepository->findAllForBackendList($list);
            $list['countAll'] = $this->glossaryCategoryRepository->findAllForBackendList($list, true);
            $this->backendListUtility->exportAsCsv($categories, $list);
        } else {
            $list['pid'] = $this->pageUid;
            $categories = $this->glossaryCategoryRepository->findAllForBackendList($list);
            $list['countAll'] = $this->glossaryCategoryRepository->findAllForBackendList($list, true);
        }
        //
        $this->getViewToUse()->assign('list', $list);
        $this->getViewToUse()->assign('categories', $categories);
        $this->getViewToUse()->assign('currentPage', $this->pageUid);
        $this->getViewToUse()->assign('actionMethodName', $this->actionMethodName);
        return $this->renderViewToUse();
    }
}
