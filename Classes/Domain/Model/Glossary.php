<?php

namespace CodingMs\Glossaries\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\CreationDateTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\DescriptionTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\HiddenTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\HtmlTitleTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaAbstractTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaDescriptionTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaKeywordsTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\ModificationDateTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\TitleTrait;
use CodingMs\Modules\Domain\Model\Traits\CheckMethodTrait;
use CodingMs\Modules\Domain\Model\Traits\ToCsvArrayTrait;
use TYPO3\CMS\Extbase\Annotation as Extbase;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class Glossary extends AbstractEntity
{
    use CheckMethodTrait;
    use ToCsvArrayTrait;
    use CreationDateTrait;
    use ModificationDateTrait;
    use HiddenTrait;
    use TitleTrait;
    use DescriptionTrait;
    use HtmlTitleTrait;
    use MetaDescriptionTrait;
    use MetaAbstractTrait;
    use MetaKeywordsTrait;

    /**
     * @var string
     */
    protected $subTitle;

    /**
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $teaser;

    /**
     * @var bool
     * @Extbase\Validate("NotEmpty")
     */
    protected $useDetailView;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Glossaries\Domain\Model\GlossaryCategory>
     */
    protected $categories;

    /**
     * @var string
     */
    protected $views;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @Extbase\ORM\Lazy
     * @Extbase\ORM\Cascade("remove")
     */
    protected $images;

    /**
     * @return Glossary
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
        return $this;
    }

    /**
     * Initializes all ObjectStorage properties.
     */
    protected function initStorageObjects()
    {
        /**
         * Do not modify this method!
         * It will be rewritten on each save in the extension builder
         * You may modify the constructor of this class instead
         */
        $this->categories = new ObjectStorage();
        $this->images = new ObjectStorage();
    }

    /**
     * Returns the subTitle
     *
     * @return string $subTitle
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Sets the subTitle
     *
     * @param string $subTitle
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;
    }

    /**
     * Returns the teaser
     *
     * @return string $teaser
     */
    public function getTeaser()
    {
        return $this->teaser;
    }

    /**
     * Sets the teaser
     *
     * @param string $teaser
     */
    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;
    }

    /**
     * Returns the useDetailView;
     *
     * @return string $useDetailView;
     */
    public function getUseDetailView()
    {
        return $this->useDetailView;
    }

    /**
     * Sets the useDetailView
     *
     * @param bool $useDetailView
     */
    public function setUseDetailView($useDetailView)
    {
        $this->useDetailView = $useDetailView;
    }

    /**
     * Adds a GlossaryCategory
     *
     * @param \CodingMs\Glossaries\Domain\Model\GlossaryCategory $category
     */
    public function addCategory(GlossaryCategory $category)
    {
        $this->categories->attach($category);
    }

    /**
     * Removes a GlossaryCategory
     *
     * @param \CodingMs\Glossaries\Domain\Model\GlossaryCategory $categoryToRemove The GlossaryCategory to be removed
     */
    public function removeCategory(GlossaryCategory $categoryToRemove)
    {
        $this->categories->detach($categoryToRemove);
    }

    /**
     * Returns the categories
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Glossaries\Domain\Model\GlossaryCategory> $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Sets the categories
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Glossaries\Domain\Model\GlossaryCategory> $categories
     */
    public function setCategories(ObjectStorage $categories)
    {
        $this->categories = $categories;
    }

    /**
     * Returns the categories as a String
     *
     * @return string
     */
    public function getCategoriesString()
    {
        $categoriesArray = [];
        $categories = $this->getCategories();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $categoriesArray[] = $category->getTitle();
            }
        }
        return implode(', ', $categoriesArray);
    }

    /**
     * @return string
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param string $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    public function increaseViews()
    {
        $this->views += 1;
    }

    /**
     * Adds a image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function addImage(FileReference $image)
    {
        $this->images->attach($image);
    }

    /**
     * Removes a image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove The image to be removed
     */
    public function removeImage(FileReference $imageToRemove)
    {
        $this->images->detach($imageToRemove);
    }

    /**
     * Returns the images
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the images
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
     */
    public function setImages(ObjectStorage $images)
    {
        $this->images = $images;
    }

    /**
     * Returns all images marked as preview
     * @return array $images
     */
    public function getImagesPreviewOnly()
    {
        $images = [];
        if ($this->getImages()) {
            /** @var \CodingMs\Glossaries\Domain\Model\FileReference $mediaItem */
            foreach ($this->getImages() as $mediaItem) {
                if ($mediaItem->getOriginalResource()->getProperty('preview')) {
                    $images[] = $mediaItem;
                }
            }
        }
        return $images;
    }
}
