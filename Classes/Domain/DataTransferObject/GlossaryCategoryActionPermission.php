<?php

namespace CodingMs\Glossaries\Domain\DataTransferObject;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Modules\Domain\DataTransferObject\AbstractPermission;
use CodingMs\Modules\Domain\DataTransferObject\PermissionTrait;

/**
 * DTO: Permission access Glossary Categories Actions
 */
final class GlossaryCategoryActionPermission extends AbstractPermission
{
    use PermissionTrait;

    /**
     * @var string
     */
    public const KEY = 'modules_glossary_category';

    /**
     * Disabled actions
     */
    public const ACTION_LIST_CATEGORIES = 'ACTION_LIST_CATEGORIES';
    public const ACTION_ADD_CATEGORY = 'ACTION_ADD_CATEGORY';
    public const ACTION_DELETE_CATEGORY = 'ACTION_DELETE_CATEGORY';

    /**
     * @return array<string, mixed>
     */
    public static function getItems(): array
    {
        return [
            self::ACTION_ADD_CATEGORY => [
                'LLL:EXT:glossaries/Resources/Private/Language/locallang.xlf:tx_glossaries_authorization.glossary_category_add_category_title',
                'module-glossaries',
            ],
            self::ACTION_DELETE_CATEGORY => [
                'LLL:EXT:glossaries/Resources/Private/Language/locallang.xlf:tx_glossaries_authorization.glossary_category_delete_category_title',
                'module-glossaries',
            ],
            self::ACTION_LIST_CATEGORIES => [
                'LLL:EXT:glossaries/Resources/Private/Language/locallang.xlf:tx_glossaries_authorization.glossary_category_list_categories_title',
                'module-glossaries',
            ],
        ];
    }

    protected function populateData(): void
    {
        $this->data = [
            'header' => 'LLL:EXT:glossaries/Resources/Private/Language/locallang.xlf:tx_glossaries_authorization.glossary_category_header',
            'items' => self::getItems(),
        ];
    }

    /** Helper classes for readability */

    /**
     * @return bool
     */
    public static function categoryCreationAllowed(): bool
    {
        return static::isConfigured(self::ACTION_ADD_CATEGORY);
    }

    /**
     * @return bool
     */
    public static function categoryDeletionAllowed(): bool
    {
        return static::isConfigured(self::ACTION_DELETE_CATEGORY);
    }

    /**
     * @return bool
     */
    public static function listCategoriesAllowed(): bool
    {
        return static::isConfigured(self::ACTION_LIST_CATEGORIES);
    }
}
