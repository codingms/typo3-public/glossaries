<?php

namespace CodingMs\Glossaries\Domain\Repository;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Glossaries\Domain\Model\GlossaryCategory;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class GlossaryRepository extends Repository
{

    /**
     * Searches some data
     *
     * @param mixed[] $searchFields
     * @param GlossaryCategory|null $selectedCategory
     * @return mixed[]|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findAllBySearch(array $searchFields = [], GlossaryCategory $selectedCategory = null)
    {
        $query = $this->createQuery();
        $constraints = [];
        $constraintsOr = [];
        // processes the search word
        if (isset($searchFields['searchWord']) && trim($searchFields['searchWord']) != '') {
            $constraintsOr[] = $query->like('title', '%' . $searchFields['searchWord'] . '%');
            $constraintsOr[] = $query->like('subTitle', '%' . $searchFields['searchWord'] . '%');
            $constraintsOr[] = $query->like('teaser', '%' . $searchFields['searchWord'] . '%');
            $constraintsOr[] = $query->like('description', '%' . $searchFields['searchWord'] . '%');
        }
        if (!empty($constraintsOr)) {
            $constraints[] = $query->logicalOr(...$constraintsOr);
        }
        // processes the category
        if ($selectedCategory instanceof GlossaryCategory && !$searchFields['ignoreCategories']) {
            $constraints[] = $query->contains('categories', $selectedCategory);
        }
        if (!empty($constraints)) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        }
        //
        // Limit
        if (isset($searchFields['limit'])) {
            $limit = (int)$searchFields['limit'];
            if ($limit > 0) {
                $query->setLimit($limit);
            }
        }
        //
        // Sorting
        if (isset($searchFields['sortBy']) && $searchFields['sortBy'] === 'views') {
            $query->setOrderings(['views' => QueryInterface::ORDER_DESCENDING]);
        } else {
            $query->setOrderings(['title' => QueryInterface::ORDER_ASCENDING]);
        }
        return $query->execute();
    }

    /**
     * @param mixed[] $filter
     * @param bool $count
     * @return array|QueryResultInterface|int
     * @throws InvalidQueryException
     */
    public function findAllForBackendList(array $filter = [], $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        //
        // Filters
        $constraints = [];
        if (isset($filter['category']['selected']) && $filter['category']['selected'] > 0) {
            $constraints[] = $query->contains('categories', $filter['category']['selected']);
        }
        if ($filter['searchWord'] !== '') {
            $constraintsSearchWord = [];
            $constraintsSearchWord[] = $query->like('title', '%' . $filter['searchWord'] . '%');
            $constraintsSearchWord[] = $query->like('sub_title', '%' . $filter['searchWord'] . '%');
            $constraintsSearchWord[] = $query->like('description', '%' . $filter['searchWord'] . '%');
            $constraints[] = $query->logicalOr(...$constraintsSearchWord);
        }
        if (count($constraints) > 0) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        }
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] !== '') {
                if ($filter['sortingOrder'] == 'asc') {
                    $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } elseif ($filter['sortingOrder'] == 'desc') {
                    $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }
}
