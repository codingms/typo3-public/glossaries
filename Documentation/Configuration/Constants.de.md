# TypoScript-Konstanten Einstellungen


## Allgemein

| **Konstante**    | themes.configuration.siteName                                   |
|:-----------------|:----------------------------------------------------------------|
| **Label**        | Site Name                                                       |
| **Beschreibung** |                                                                 |
| **Typ**          | string                                                          |
| **Standardwert** |                                                                 |



## Glossar-Seiten

| **Konstante**    | themes.configuration.pages.glossary.list                        |
|:-----------------|:----------------------------------------------------------------|
| **Label**        | Page-Uid der Liste                                              |
| **Beschreibung** |                                                                 |
| **Typ**          | int+                                                            |
| **Standardwert** | 0                                                               |

| **Konstante**    | themes.configuration.pages.glossary.detail                      |
|:-----------------|:----------------------------------------------------------------|
| **Label**        | Page-Uid der Detailansicht                                      |
| **Beschreibung** |                                                                 |
| **Typ**          | int+                                                            |
| **Standardwert** | 0                                                               |



## Glossar-Container

| **Konstante**    | themes.configuration.container.glossary                         |
|:-----------------|:----------------------------------------------------------------|
| **Label**        | Container für Glossardatensätze                                 |
| **Beschreibung** |                                                                 |
| **Typ**          | int+                                                            |
| **Standardwert** | 0                                                               |



## Glossare Templates

| **Konstante**    | themes.configuration.extension.glossaries.view.templateRootPath |
|:-----------------|:----------------------------------------------------------------|
| **Label**        | Template Root-Pfad                                              |
| **Beschreibung** |                                                                 |
| **Typ**          | string                                                          |
| **Standardwert** | EXT:glossaries/Resources/Private/Templates/                     |

| **Konstante**    | themes.configuration.extension.glossaries.view.partialRootPath  |
|:-----------------|:----------------------------------------------------------------|
| **Label**        | Partial Root-Pfad                                               |
| **Beschreibung** |                                                                 |
| **Typ**          | string                                                          |
| **Standardwert** | EXT:glossaries/Resources/Private/Partials/                      |

| **Konstante**    | themes.configuration.extension.glossaries.view.layoutRootPath   |
|:-----------------|:----------------------------------------------------------------|
| **Label**        | Layout-Root-Pfad                                                |
| **Beschreibung** |                                                                 |
| **Typ**          | string                                                          |
| **Standardwert** | EXT:glossaries/Resources/Private/Layouts/                       |



