# Glossar-Erweiterung für TYPO3 installieren

1. Installiere die Erweiterung mit composer oder dem TYPO3 TER/Extensionmanager
2. Binde das statische TypoScript-Template ein
3. Konfiguriere die mitgelieferten TypoScript-Konstanten
4. Erstelle einen Glossar-Datencontainer im Seitenbaum
5. Erstelle Glossareinträge
6. Füge ein Glossar-Plugin auf einer Seite ein
