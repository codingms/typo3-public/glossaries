# Human readable URLs

In order to use human readable URLs for links to glossary categories, please add the following snippet to your site configuration. Please configure the "limitToPages" value for the page IDs you are using the plugin on.

## Routing configuration
```yaml
  GlossariesPlugin:
      type: Extbase
      limitToPages:
        - 5
      extension: Glossaries
      plugin: Glossary
      routes:
        - routePath: '/{category_slug}'
          _controller: 'Glossary::list'
          _arguments:
            category_slug: selectedCategory
        - routePath: '/{category_slug}/{glossary_slug}'
          _controller: 'Glossary::show'
          _arguments:
            category_slug: selectedCategory
            glossary_slug: glossary
      defaultController: 'Glossary::list'
      aspects:
        category_slug:
          type: PersistedAliasMapper
          tableName: tx_glossaries_domain_model_glossarycategory
          routeFieldName: slug
          routeValuePrefix: /
        glossary_slug:
          type: PersistedAliasMapper
          tableName: tx_glossaries_domain_model_glossary
          routeFieldName: slug
          routeValuePrefix: /
```
