# Sprechende URLs

Um sprechende URLs für Links zu Glossarkategorien zu verwenden, füge bitte das folgende Snippet zu Deiner Site-Konfiguration. Bitte konfiguriere den Wert "limitToPages" für die Seiten-IDs, auf der Du das Plugin verwendest.

## Routing Konfiguration
```yaml
  GlossariesPlugin:
      type: Extbase
      limitToPages:
        - 5
      extension: Glossaries
      plugin: Glossary
      routes:
        - routePath: '/{category_slug}'
          _controller: 'Glossary::list'
          _arguments:
            category_slug: selectedCategory
        - routePath: '/{category_slug}/{glossary_slug}'
          _controller: 'Glossary::show'
          _arguments:
            category_slug: selectedCategory
            glossary_slug: glossary
      defaultController: 'Glossary::list'
      aspects:
        category_slug:
          type: PersistedAliasMapper
          tableName: tx_glossaries_domain_model_glossarycategory
          routeFieldName: slug
          routeValuePrefix: /
        glossary_slug:
          type: PersistedAliasMapper
          tableName: tx_glossaries_domain_model_glossary
          routeFieldName: slug
          routeValuePrefix: /
```
