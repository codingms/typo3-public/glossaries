# Breadcrumbs

```typo3_typoscript

# Breadcrumb settings
# ATTENTION: In case of using TYPO3-THEMES, this TypoScript must be placed directly with in the Theme or in the Root-Template!
lib.menu.breadcrumb.10.1.wrap >
lib.menu.breadcrumb.5 = TEXT
lib.menu.breadcrumb.5.value = <ul id="menuBreadcrumb" class="{$themes.configuration.menu.breadcrumb.cssClasses}"><li class="prefix"><span>{$themes.configuration.menu.breadcrumb.prefix}</span></li>
[globalVar = GP:tx_glossaries_glossary|glossary > 0]
	# Do not mark the last menu entry as active!
	lib.menu.breadcrumb.10.1.NO.linkWrap = <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">|</li> |*| <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span class="divider">{$themes.configuration.menu.breadcrumb.divider}</span>|</li> |*| <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span class="divider">{$themes.configuration.menu.breadcrumb.divider}</span>|</li>
	lib.menu.breadcrumb.10.1.NO.doNotLinkIt = |*| 0 |*| 0
	# Set record as last entry
	lib.menu.breadcrumb.20 = RECORDS
	lib.menu.breadcrumb.20 {
		dontCheckPid = 1
		tables = tx_glossaries_domain_model_glossary
		source.data = GP:tx_glossaries_glossary|glossary
		source.intval = 1
		conf.tx_glossaries_domain_model_glossary = TEXT
		conf.tx_glossaries_domain_model_glossary.field = title
		conf.tx_glossaries_domain_model_glossary.htmlSpecialChars = 1
		wrap = <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="active"><span class="divider">{$themes.configuration.menu.breadcrumb.divider}</span><span itemprop="title">|</span></li>
	}
[end]
lib.menu.breadcrumb.90 = TEXT
lib.menu.breadcrumb.90.value = </ul>

```
