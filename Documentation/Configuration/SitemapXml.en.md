# Sitemap.xml configuration for glossaries

The following configuration enables you to generate a Sitemap.xml for your products:

```typo3_typoscript
glossaries {
	provider = TYPO3\CMS\Seo\XmlSitemap\RecordsXmlSitemapDataProvider
	config {
		table = tx_glossaries_domain_model_glossarycategory
		sortField = sorting
		lastModifiedField = tstamp
		recursive = 1
		# Speicherort der Beiträge
		pid = 1200
		url {
			# PageID der Detailseite
			pageId = 1982
			fieldToParameterMap {
				uid = tx_glossaries_glossary[selectedCategory]
			}
			additionalGetParameters {
				tx_glossaries_glossary.controller = Glossary
				tx_glossaries_glossary.action = list
			}
			useCacheHash = 1
		}
	}
}
```
