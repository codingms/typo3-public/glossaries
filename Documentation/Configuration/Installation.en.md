# Install the Glossar extension for TYPO3

1. Install the extension by using composer or TYPO3 TER/Extensionmanager
2. Include the static TypoScript-Template
3. Configure the shipped TypoScript constants
4. Create a Glossar data container in your page tree
5. Create your glossar entry records
6. Insert a glossar plugin on any page
