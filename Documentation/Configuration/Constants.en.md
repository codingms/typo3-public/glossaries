# TypoScript constant settings


## General

| **Constant**      | themes.configuration.siteName                                   |
| :---------------- | :-------------------------------------------------------------- |
| **Label**         | Site name                                                       |
| **Description**   |                                                                 |
| **Type**          | string                                                          |
| **Default value** |                                                                 |



## Glossary Pages

| **Constant**      | themes.configuration.pages.glossary.list                        |
| :---------------- | :-------------------------------------------------------------- |
| **Label**         | Page-Uid of the list                                            |
| **Description**   |                                                                 |
| **Type**          | int+                                                            |
| **Default value** | 0                                                               |

| **Constant**      | themes.configuration.pages.glossary.detail                      |
| :---------------- | :-------------------------------------------------------------- |
| **Label**         | Page-Uid of the detail view                                     |
| **Description**   |                                                                 |
| **Type**          | int+                                                            |
| **Default value** | 0                                                               |



## Glossary Container

| **Constant**      | themes.configuration.container.glossary                         |
| :---------------- | :-------------------------------------------------------------- |
| **Label**         | Container for glossary records                                  |
| **Description**   |                                                                 |
| **Type**          | int+                                                            |
| **Default value** | 0                                                               |



## Glossaries Templates

| **Constant**      | themes.configuration.extension.glossaries.view.templateRootPath |
| :---------------- | :-------------------------------------------------------------- |
| **Label**         | Template root path                                              |
| **Description**   |                                                                 |
| **Type**          | string                                                          |
| **Default value** | EXT:glossaries/Resources/Private/Templates/                     |

| **Constant**      | themes.configuration.extension.glossaries.view.partialRootPath  |
| :---------------- | :-------------------------------------------------------------- |
| **Label**         | Partial root path                                               |
| **Description**   |                                                                 |
| **Type**          | string                                                          |
| **Default value** | EXT:glossaries/Resources/Private/Partials/                      |

| **Constant**      | themes.configuration.extension.glossaries.view.layoutRootPath   |
| :---------------- | :-------------------------------------------------------------- |
| **Label**         | Layout root path                                                |
| **Description**   |                                                                 |
| **Type**          | string                                                          |
| **Default value** | EXT:glossaries/Resources/Private/Layouts/                       |



