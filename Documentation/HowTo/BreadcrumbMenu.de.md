# Breadcrumb Menu

Um ein zusätzliches Element zu einem Breadcrumb-Menü auf Glossar-Detailseiten hinzuzufügen, kannst Du den `AddGlossaryToMenuProcessor` verwenden.
Füge dazu das folgende TypoScript in den Setup-Abschnitt Deiner Site-Package-Erweiterung ein. Wir gehen hier davon aus, dass sich Dein Haupt `FLUIDTEMPLATE` in `page.10` befindet.

```typo3_typoscript
page.10 = FLUIDTEMPLATE
page.10 {
    # [...] template settings
    dataProcessing {
        # [...] Other data processors
        50 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
        50 {
            as = breadcrumbMenu
            special = rootline
        }
        60 = CodingMs\Glossaries\DataProcessing\AddGlossaryToMenuProcessor
        60.menus = breadcrumbMenu
    }
}
```

Das Feld `menus` des `AddGlossaryToMenuProcessor` muss den Schlüssel des `MenuProcessor` enthalten, der den Breadcrumb enthält. Du kannst hier mehr als ein Menü verwenden, indem Du mehrere Schlüssel als kommagetrennte Liste angibst. Zum Beispiel: `60.menus = breadcrumbMenu,myOtherBreadcrumb`.
