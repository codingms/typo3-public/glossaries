# How to use teaser field as rich-text-editor field

Just add the following file (`EXT:site_package/Configuration/TCA/Overrides/tx_glossaries_domain_model_glossary.php`) to your site package extension:

```php
<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$GLOBALS['TCA']['tx_glossaries_domain_model_glossary']['columns']['teaser']['defaultExtras'] = 'richtext:rte_transform[flag=rte_enabled|mode=ts]';
$GLOBALS['TCA']['tx_glossaries_domain_model_glossary']['columns']['teaser']['config'] = \CodingMs\AdditionalTca\Tca\Configuration::get('rte');
```
