# Wie kann ich das Teaser-Feld als Rich-Text-Feld nutzen

Füge einfach die folgende Datei (`EXT:site_package/Configuration/TCA/Overrides/tx_glossaries_domain_model_glossary.php`) in deine Site-Package Erweiterung ein:

```php
<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$GLOBALS['TCA']['tx_glossaries_domain_model_glossary']['columns']['teaser']['defaultExtras'] = 'richtext:rte_transform[flag=rte_enabled|mode=ts]';
$GLOBALS['TCA']['tx_glossaries_domain_model_glossary']['columns']['teaser']['config'] = \CodingMs\AdditionalTca\Tca\Configuration::get('rte');
```
